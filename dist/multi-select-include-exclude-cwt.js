'use strict';
(function (module) {
  try {
    module = angular.module('cwt.multi-select-include-exclude');
  } catch (e) {
    module = angular.module('cwt.multi-select-include-exclude', []);
  }
  var directiveName = 'cwtMultiSelectIncludeExclude';
  var theDirective = function ($timeout) {
    return {
      restrict: 'E',
      templateUrl: 'templates/multiselect-include-exclude.html',
      require: ['ngModel', 'cwtMultiSelectIncludeExclude'],
      controllerAs: 'ctrlMultiSelectIncludeExclude',
      scope: {
        ngModel: '=',
        emptyText: '@',
        disabled: '=',
        isRequired: '=',
        loadingData: '=',
        readMode: '=',
        source: '=',
        name: '@',
        isflat: '@?',
        displayLocation: '@?',
        showSearchbar: '=?',
        displayProperty: '@?',
        hasError: '=?',
        errorMessage: '@',
        errorPlacement: '@',
        selectAllDisabled:'=?',
        onlyInclude: '=',
        onlyExclude: '=',
        firstPickSticks: '=',
        uniqueId:'@',
        observationMode: '=?'
      },
      link: function (scope, element, attrs, controller) {
        scope.safeApply = function (fn) {
          var phase = this.$root.$$phase;
          if (phase === '$apply' || phase === '$digest') {
            if (fn && (typeof (fn) === 'function')) {
              fn();
            }
          } else {
            this.$apply(fn);
          }
        };
        controller[1].setNgModelController(controller[0]);
      },
      controller: ['$scope', '$element', '$timeout', function ($scope, $element, $timeout) {
        var ctrl = this,
          modelCtrl = null;

        ctrl.isInclude = true;
        if (ctrl.ngModel === undefined) {
          ctrl.ngModel = [];
        }

        ctrl.setNgModelController = function (model) {
          modelCtrl = model;
        };

        if (!$scope.selectAllLabel) {
          $scope.selectAllLabel = 'Select all';
        }
        if (!$scope.selectAllMessage) {
          $scope.selectAllMessage = 'everything is selected';
        }

        function ngModelWatch(value) {
          if ($scope.sourceCopy) {
            $scope.traversData = '';
            if ($scope.isflat) {
              $scope.traversData = $scope.sourceCopy;
            } else {
              $scope.traversData = getAllchilds($scope.sourceCopy);
            }

            if (value && $scope.traversData) {
              var hulpArray = angular.copy(value);
              for (var i = 0; i < $scope.traversData.length; i++) {

                var exists = _.find(value, function(val) {
                  return val[$scope.uniqueId] === $scope.traversData[i][$scope.uniqueId];
                });
                var index = -1;
                if (exists) {
                  index = value.indexOf(exists);
                }

                var existsHulp = _.find(hulpArray, function(val) {
                  return val[$scope.uniqueId] === $scope.traversData[i][$scope.uniqueId];
                });
                var indexHulp = -1;
                if (existsHulp) {
                  indexHulp = hulpArray.indexOf(existsHulp);
                }

                if (index > -1) {
                  hulpArray.splice(indexHulp, 1);
                  var uniqueId = value[index][$scope.uniqueId];
                  var item = $scope.traversData[i];
                  if (item) {
                    item.included = value[index].included;
                    item.isChecked = true;
                  } else {
                    var ngModelObject = _.find($scope.ngModel, function (mod) {
                      return mod[$scope.uniqueId] === uniqueId;
                    })
                    var index = -1;
                    if (ngModelObject) {
                      index = $scope.ngModel.indexOf(ngModelObject);
                    }
                    $scope.ngModel.splice(index, 1);
                  }
                } else {
                  $scope.traversData[i].isChecked = false;
                }
              }
              if (!$scope.isflat) {
                checkparent($scope.sourceCopy);
              }
              if (hulpArray.length > 0) {
                for (var i = 0; i < hulpArray.length; i++) {
                  var ngModelObject = _.find($scope.ngModel, function (mod) {
                    return mod[$scope.uniqueId] === hulpArray[i][$scope.uniqueId];
                  })
                  var index = -1;
                  if (ngModelObject) {
                    index = $scope.ngModel.indexOf(ngModelObject);
                  }
                  $scope.ngModel.splice(index, 1);
                }
              }
            }
          }
        }

        $scope.$watch('source', function () {
          $scope.sourceCopy = angular.copy($scope.source);
          ngModelWatch($scope.ngModel)
          if (!$scope.isflat) {
            _.forEach($scope.sourceCopy, function (child) {
              if (child.children && child.children.length !== 0) {
                child.isClosed = true;
              }
            });
          }
        })

        $scope.$watch('ngModel', ngModelWatch);

        ctrl.selectedItems = function () {
          if (!$scope.ngModel) {
            return [];
          }
          $scope.displayList = [];
          _.each($scope.ngModel, function (item) {
            if ($scope.traversData) {
              $scope.displayList.push(find(item, $scope.traversData));
            }
          })
          return $scope.ngModel;
        }
        function find(item, items, run) {
          var i = 0, found;
          if (items) {
            for (var i = 0; i < items.length; i++) {
              if (items[i][$scope.uniqueId] === item[$scope.uniqueId]) {
                if (run) {
                  items[i].isChecked = false;
                }
                return items[i];
              } else if (_.isArray(items[i].children)) {
                found = find(item[$scope.uniqueId], items[i].children);
                if (found) {
                  return found;
                }
              }
            }
          } else {
            return null;
          }
        }

        function getAllchilds(item, childs, force) {
          var hulp = [];
          _.forEach(item, function (child) {
            if (child.children && !force && child.children.length > 0) {
              child.hasChildren = child.children.length > 0;
              var children = getAllchilds(child.children, childs);
              if (children.length > 0) {
                hulp = hulp.concat(children);
              }

            } else {
              hulp.push(child);
            }
          });
          return hulp;
        }

        function SetState(item, state) {
          if (item) {
            item.indeterminate = false;
            item.isChecked = false;
            if (state === 'indeterminate') {
              item.indeterminate = true;
            } else if (state === 'isChecked') {
              item.isChecked = true;
            }
          }
        }

        function checkparent(item, check) {
          var hulp = [];
          _.forEach(item, function (child) {
            if (child.children) {
              var children = checkparent(child.children, check);
              var selected = _.filter(children, function (item) {
                return item.isChecked;
              });
              if (selected.length === child.children.length && child.children.length !== 0) {
                SetState(child, 'isChecked');
              } else if (selected.length === child.children.length && child.children.length === 0) {
                var hulpState = child.isChecked;
                SetState(child);
                child.isChecked = hulpState;
              } else if (selected.length > 0) {
                SetState(child, 'indeterminate');
              } else {
                SetState(child);
              }
            }
            if (child.isChecked && !child.children && check === true && checkedItems.indexOf(child[$scope.uniqueId]) < 0) {
              checkedItems.push(child[$scope.uniqueId]);
            }
            //if(selected)
            hulp.push(child);
          });
          return hulp;
        }

        ctrl.onSelectItem = function (item, value, force) {

          if (item.children && item.children.length > 0) {
            handle(getAllchilds([item], [], false), true, item.isChecked);
          } else {
            handle([item], force, item.isChecked);
          }
          // Clear search 
          ctrl.searchcrit = '';
          $scope.search = ''
          // focus search
          var searchbar = angular.element('#searchbar');
          if (searchbar) searchbar.focus();
          if (item.children && item.children.length === 0) {
            return;
          }
          if (!$scope.isflat) {
            checkparent($scope.sourceCopy);
          }
          
        }
        ctrl.checkAll = function () {
          for (var i = 0; i < $scope.sourceCopy.length; i++) {
            $scope.sourceCopy[i].isChecked = true;
          }
          if ($scope.isflat) {
            handle($scope.sourceCopy, true, true);
          } else {
            var items = getAllchilds($scope.sourceCopy, [], false);
            handle(items, true, true);
            checkparent($scope.sourceCopy);
          }
        }

        ctrl.unCheckAll = function () {
          for (var i = 0; i < $scope.sourceCopy.length; i++) {
            $scope.sourceCopy[i].isChecked = false;
          }
          if ($scope.isflat) {
            handle($scope.sourceCopy, true, false);
          } else {
            var items = getAllchilds($scope.sourceCopy, [], false);
            handle(items, true, false);
            checkparent($scope.sourceCopy);
          }
        }
        ctrl.showOnlySelected = false;
        ctrl.showSelected = function () {
          ctrl.showOnlySelected = !ctrl.showOnlySelected;
        }
        ctrl.showAll = function () {
          ctrl.showOnlySelected = !ctrl.showOnlySelected;
        }

        ctrl.updateSearch = function (txt) {
          if (txt.length > 1) {
            ctrl.searchcrit = txt;
          } else if (txt.length === 0) {
            ctrl.searchcrit = '';
          }
        };

        function handle(items, force, forceValue) {
          var newModel = angular.copy($scope.ngModel);
          items.map(function(item) {
            item.isNew = true
          })
          for (var i = 0; i < items.length; i++) {

            items[i].included = angular.copy(ctrl.isInclude);
            if (force) {
              items[i].isChecked = forceValue;
            }
            var newModelObject = _.find(newModel, function(mod) {
              return mod[$scope.uniqueId] === items[i][$scope.uniqueId];
            });

            var index = -1;
            if (newModelObject) {
              index = newModel.indexOf(newModelObject);
            }

            if (force) {
              if (index > -1 && !forceValue) {
                newModel.splice(index, 1);
              } else if (index === -1 && forceValue) {
                newModel.push(items[i]);
              }
            } else if (index > -1) {
              newModel.splice(index, 1);
            } else {
              newModel.push(items[i]);
            }
          }
          modelCtrl.$setViewValue(newModel);
          modelCtrl.$setDirty();
        }

        //thi click elementç
        ctrl.clickHandler = function (event, doc, isInclude) {
          if (isInclude !== undefined && isInclude !== null) {
            ctrl.isInclude = isInclude;
          }
          $(document).unbind('click', ctrl.clickHandler);

          $scope.safeApply(function () {
            var isClickedElementChildOfPopup = $element
              .find(event.target)
              .length > 0;
            var isPopover = $(event.target).closest('.popover').length > 0;

            if (isClickedElementChildOfPopup) {
              if (ctrl.showSearchbar === 'true' && ctrl.editMode) {
                $timeout(function () {
                  $element.find('#searchbar').focus();
                }, 150);
              }
              if (!isPopover) {
                ctrl.editMode = !ctrl.editMode;
              }
              $timeout(function () {
                $(document).bind('click', ctrl.clickHandler);
              }, 250);
            } else {
              $(document).unbind('click', ctrl.clickHandler);
              ctrl.editMode = false;
            }
          });
        };

      }]
    };
  };
  theDirective.$inject = ['$timeout'];
  module.directive(directiveName, theDirective);
})(null);
;'use strict';
(function (module) {
    try {
        module = angular.module('cwt.multi-select-include-exclude');
    } catch (e) {
        module = angular.module('cwt.multi-select-include-exclude', []);
    }
    var directiveName = 'virtualListIncludeExclude';
    var theDirective = function ($compile) {
        return {
            restrict: 'E',
            require: 'ngModel',
            templateUrl: 'templates/ui-virtual-list-include-exclude.html',
            scope: {
                uiDataProvider: '=',
                onlySelected: '=',
                searchCrit: '=',
                onSelect: '&',
                readonlyMode: '=',
                disabled: '=',
                ngModel: '=',
                displayProperty: '@'
            },
            link: function (scope, elem, attrs, ngModelCtrl) {
                var rowHeight = 30;
                scope.height = 200;
                scope.scrollTop = 0;
                scope.visibleProvider = [];
                scope.cellsPerPage = 0;
                scope.numberOfCells = 0;
                scope.canvasHeight = {};

                var allChilds;

                function update() {
                    allChilds = getAllchilds(scope.uiDataProvider, [], 0);
                    if (scope.selectAll) {
                        scope.selectAll = false;
                        _.each(allChilds, function (child) {
                            scope.selectItem(child);
                        });
                    } else {
                        scope.init();
                    }
                }


                scope.$watch('uiDataProvider', function ui() {
                    update();
                });
                scope.$watch('onlySelected', function onlyselected() {
                    update();
                });
                scope.$watch('searchCrit', function searchcrit() {
                    update();
                });
                scope.$watch('ngModel', function searchcrit() {
                    update();
                });

                // Init
                scope.init = function () {
                    elem[0].removeEventListener('scroll', scope.onScroll);
                    elem[0].addEventListener('scroll', scope.onScroll);
                    if (allChilds) {
                        scope.cellsPerPage = Math.round(scope.height / rowHeight);
                        scope.numberOfCells = 3 * scope.cellsPerPage;
                        scope.canvasHeight = {
                            height: allChilds.length * rowHeight + 'px'
                        };

                        scope.updateDisplayList();
                    }
                };

                scope.updateDisplayList = function () {
                    var firstCell = Math.max(Math.floor(scope.scrollTop / rowHeight) - scope.cellsPerPage, 0);
                    var cellsToCreate = Math.min(firstCell + scope.numberOfCells, scope.numberOfCells);
                    scope.visibleProvider = allChilds.slice(firstCell, firstCell + cellsToCreate);

                    for (var i = 0; i < scope.visibleProvider.length; i++) {
                        scope.visibleProvider[i].styles = {
                            'top': ((firstCell + i) * rowHeight) + 'px'
                        };
                    }
                };

                function checkChildren(item, state) {
                    if (item.children && item.children.length > 0) {
                        _.forEach(item.children, function (child) {
                            child.isChecked = item.isChecked;
                            if (child.children) {
                                checkChildren(child, state);
                            }
                        });
                    }
                }

                scope.selectItem = function (item) {
                    if (!scope.disabled && !scope.readonlyMode) {
                        item.isChecked = !item.isChecked;
                        checkChildren(item, item.isChecked);
                        scope.onSelect({ item: item, value: item.isChecked});
                        scope.updateDisplayList();
                    }
                };

                scope.openClose = function ($event, item) {
                    item.isClosed = !item.isClosed;
                    allChilds = getAllchilds(scope.uiDataProvider, [], 0);
                    scope.init();
                    $event.stopPropagation();
                    $event.preventDefault();
                };
                function getAllchilds(item, childs, index, force) {
                    var hulp = [];
                    _.forEach(item, function (child) {
                        child.index = index;
                        if (child.children && child.children.length !== 0 && !force) {
                            child.hasChildren = child.children.length > 0;
                            if (scope.searchCrit) {
                                child.isClosed = false;
                            }

                            if (scope.onlySelected && !scope.readonlyMode) {
                                child.isClosed = false;
                            } else if (child.isClosed && !scope.readonlyMode) {
                                var test = getAllchilds([child], [], index, true);
                                if (test.length === 1) {
                                    hulp.push(child);
                                }
                                return;
                            }
                            var children = getAllchilds(child.children, childs, index + 1);
                            if (children.length > 0) {
                                if (!scope.readonlyMode) {
                                    hulp.push(child);
                                }
                                hulp = hulp.concat(children);
                            } else if (child.children.length === 0) {
                                if (scope.onlySelected || scope.readonlyMode) {
                                    if (child.isChecked) {
                                        hulp = hulp.concat(child);
                                    }
                                } else {
                                    hulp = hulp.concat(child);
                                }
                            }

                        } else {
                            if (scope.onlySelected || scope.readonlyMode) {
                                if (!child.isChecked) {
                                    return;
                                }
                            }
                            else {
                                if (child.isChecked && !scope.searchCrit) {
                                    return;
                                }
                            }
                            if (scope.searchCrit && scope.searchCrit !== '') {
                                if (child && child[scope.displayProperty] && child[scope.displayProperty].toLowerCase().indexOf(scope.searchCrit.toLowerCase()) === -1) {
                                    return;
                                }
                                if (child && !child[scope.displayProperty]) {
                                    return;
                                }
                            }
                            hulp.push(child);
                        }
                    });
                    return hulp;
                }
                scope.$on('destroy', function () {
                    elem[0].removeEventListener('scroll', scope.onScroll);
                });
                scope.onScroll = function (evt) {
                    scope.$apply(function () {
                        scope.scrollTop = elem.prop('scrollTop');
                        scope.updateDisplayList();
                    });
                };

                scope.init();
            }
        };
    };
    theDirective.$inject = ['$compile'];
    module.directive(directiveName, theDirective);
})(null);
;angular.module('cwt.multi-select-include-exclude').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('templates/multiselect-include-exclude.html',
    "<div class=multiselect-include-exclude ng-class=\"{'multiselectreadonly': readMode, 'has-error': hasError }\">\n" +
    "<div ng-hide=!loadingData class=\"form-control loading\" ng-class=\"{'readonly': readMode }\"><i class=\"fa fa-spinner fa-pulse fa-fw\"></i></div>\n" +
    "<div ng-show=\"!loadingData && allSelected && readMode\" style=\"height: 34px;line-height:34px\">{{::selectAllMessage}}</div>\n" +
    "<div ng-show=\"!loadingData && allSelected && !readMode\" style=\"height: 34px;line-height:34px\"></div>\n" +
    "<div ng-show=\"!loadingData && !allSelected\" class=\"form-control multiselect-body\" ng-class=\"{'multiselectreadonly': readMode}\" ng-click=$event.preventDefault();$event.stopPropagation(); uib-popover={{errorMessage}} popover-trigger=\"'mouseenter'\" popover-placement={{errorPlacement}} popover-enable=hasError>\n" +
    "<span class=placeholder style=\"height: 34px\" data-ng-if=\"ctrlMultiSelectIncludeExclude.selectedItems().length <= 0\">{{::emptyText}}</span>\n" +
    "<div ng-repeat=\"item in displayList | filter: {'isNew': true} | orderBy : displayProperty\" ng-show=\"!loadingData && !allSelected && !readMode\" class=\"multiselect-badge form-control new-item\" ng-class=\"{'isIncluded': item.included || item.included === undefined,'isExcluded': !item.included && item.included != undefined}\" ng-click=$event.preventDefault();$event.stopPropagation();>\n" +
    "<span>{{item[displayProperty]}}</span>\n" +
    "<i class=\"fa fa-times\" ng-click=ctrlMultiSelectIncludeExclude.onSelectItem(item,false);$event.preventDefault();$event.stopPropagation();></i>\n" +
    "</div>\n" +
    "<div ng-repeat=\"item in displayList | filter: {'isNew': '!'} | orderBy : displayProperty\" ng-show=\"!loadingData && !allSelected && !readMode\" class=\"multiselect-badge form-control\" ng-class=\"{'isIncluded': item.included || item.included === undefined,'isExcluded': !item.included && item.included != undefined}\" ng-click=$event.preventDefault();$event.stopPropagation();>\n" +
    "<span>{{item[displayProperty]}}</span>\n" +
    "<i class=\"fa fa-times\" ng-click=ctrlMultiSelectIncludeExclude.onSelectItem(item,false);$event.preventDefault();$event.stopPropagation();></i>\n" +
    "</div>\n" +
    "<span data-ng-if=\"ctrlMultiSelectIncludeExclude.selectedItems().length > 0 && readMode\" ng-click=$event.preventDefault();$event.stopPropagation();>\n" +
    "<div ng-repeat=\"item in displayList | orderBy : displayProperty\" class=\"multiselect-badge form-control readonly\" ng-click=$event.preventDefault();$event.stopPropagation(); ng-class=\"{'isIncluded': item.included || item.included === undefined,'isExcluded': !item.included && item.included != undefined}\">\n" +
    "<span>{{item[displayProperty]}}</span>\n" +
    "</div>\n" +
    "</span>\n" +
    "</div>\n" +
    "<div class=multiselect-button-ribbon style=width:100% data-ng-if=!readMode>\n" +
    "<button type=button class=\"button button--50percent\" ng-click=\"ctrlMultiSelectIncludeExclude.clickHandler($event,'dom', true);\" ng-hide=\"onlyExclude || onlyInclude || firstPickSticks\">Include</button>\n" +
    "<button type=button class=\"button button--50percent\" ng-click=\"ctrlMultiSelectIncludeExclude.clickHandler($event,'dom', false);\" ng-hide=\"onlyExclude || onlyInclude || firstPickSticks\">Exclude</button>\n" +
    "<button type=button class=\"button button--50percent\" ng-class=\"{'button--100percent': onlyInclude || (firstPickSticks && ctrlMultiSelectIncludeExclude.selectedItems().length > 0 && (ctrlMultiSelectIncludeExclude.selectedItems()[0].included || ctrlMultiSelectIncludeExclude.selectedItems()[0].included === undefined))}\" ng-hide=\"onlyExclude || firstPickSticks || (!onlyExclude && !onlyInclude && !firstPickSticks)\" ng-click=\"ctrlMultiSelectIncludeExclude.clickHandler($event,'dom', true);\">Include</button>\n" +
    "<button type=button class=\"button button--50percent\" ng-class=\"{'button--100percent': onlyInclude || (firstPickSticks && ctrlMultiSelectIncludeExclude.selectedItems().length > 0 && (ctrlMultiSelectIncludeExclude.selectedItems()[0].included || ctrlMultiSelectIncludeExclude.selectedItems()[0].included === undefined))}\" ng-hide=\"onlyInclude || firstPickSticks || (!onlyExclude && !onlyInclude && !firstPickSticks)\" ng-click=\"ctrlMultiSelectIncludeExclude.clickHandler($event,'dom', false);\">Exclude</button>\n" +
    "<button type=button class=\"button button--50percent\" ng-class=\"{'button--100percent': onlyInclude || (firstPickSticks && ctrlMultiSelectIncludeExclude.selectedItems().length > 0 && (ctrlMultiSelectIncludeExclude.selectedItems()[0].included || ctrlMultiSelectIncludeExclude.selectedItems()[0].included === undefined))}\" ng-hide=\"(onlyExclude || onlyInclude) || !((firstPickSticks && (ctrlMultiSelectIncludeExclude.selectedItems().length <= 0 || ctrlMultiSelectIncludeExclude.selectedItems().length === undefined)) || (firstPickSticks && ctrlMultiSelectIncludeExclude.selectedItems().length > 0 && (ctrlMultiSelectIncludeExclude.selectedItems()[0].included || ctrlMultiSelectIncludeExclude.selectedItems()[0].included === undefined)))\" ng-click=\"ctrlMultiSelectIncludeExclude.clickHandler($event,'dom', true);\">Include</button>\n" +
    "<button type=button class=\"button button--50percent\" ng-class=\"{'button--100percent': onlyExclude || (firstPickSticks && ctrlMultiSelectIncludeExclude.selectedItems().length > 0 && (!ctrlMultiSelectIncludeExclude.selectedItems()[0].included && ctrlMultiSelectIncludeExclude.selectedItems()[0].included != undefined))}\" ng-hide=\"(onlyExclude || onlyInclude) || !((firstPickSticks && (ctrlMultiSelectIncludeExclude.selectedItems().length <= 0 || ctrlMultiSelectIncludeExclude.selectedItems().length === undefined)) || (firstPickSticks && ctrlMultiSelectIncludeExclude.selectedItems().length > 0 && (!ctrlMultiSelectIncludeExclude.selectedItems()[0].included && ctrlMultiSelectIncludeExclude.selectedItems()[0].included != undefined)))\" ng-click=\"ctrlMultiSelectIncludeExclude.clickHandler($event,'dom', false);\">Exclude</button>\n" +
    "<button type=button class=\"button button--100percent button--alert\" ng-click=ctrlMultiSelectIncludeExclude.unCheckAll();$event.preventDefault();$event.stopPropagation();>Clear all</button>\n" +
    "</div>\n" +
    "<div data-ng-if=\"ctrlMultiSelectIncludeExclude.editMode && !readMode && !allSelected\" class=\"popover multiselect-popover\" ng-class=\"{'left': displayLocation === 'left', 'right': displayLocation !== 'left','is-include': ctrlMultiSelectIncludeExclude.isInclude,'is-exclude': !ctrlMultiSelectIncludeExclude.isInclude}\">\n" +
    "<div data-ng-if=!selectAllDisabled class=input-group>\n" +
    "<input id=searchbar ng-disabled=disableSearch ng-if=showSearchbar ng-change=ctrlMultiSelectIncludeExclude.updateSearch(search) ng-model=$parent.$parent.$parent.search class=\"popover-search form-control\" ng-model-options={debounce:333} placeholder=Search>\n" +
    "<span class=input-group-btn>\n" +
    "<button id=single-button ng-click=ctrlMultiSelectIncludeExclude.checkAll() type=button class=\"btn btn-primary\" uib-dropdown-toggle ng-disabled=\"disabled || search\">\n" +
    "Select all\n" +
    "</button>\n" +
    "</span>\n" +
    "</div>\n" +
    "<input ng-if=\"selectAllDisabled && showSearchbar\" id=searchbar ng-disabled=disableSearch ng-change=ctrlMultiSelectIncludeExclude.updateSearch(search) ng-model=$parent.$parent.search class=\"popover-search form-control\" ng-model-options={debounce:333} placeholder=Search>\n" +
    "<virtual-list-include-exclude ui-data-provider=$parent.sourceCopy display-property={{::displayProperty}} select-all=ctrlMultiSelectIncludeExclude.selectAll disabled readonly-mode=$parent.readMode search-crit=ctrlMultiSelectIncludeExclude.searchcrit only-selected=ctrlMultiSelectIncludeExclude.showOnlySelected on-select=ctrlMultiSelectIncludeExclude.onSelectItem(item,value) ng-model=ngModel>\n" +
    "\n" +
    "</virtual-list-include-exclude></div>\n" +
    "</div>"
  );


  $templateCache.put('templates/ui-virtual-list-include-exclude.html',
    "<div class=canvas ng-style=canvasHeight>\n" +
    "<div class=renderer ng-repeat=\"item in visibleProvider\" ng-click=onClickOption(item) ng-style=item.styles ng-class=\"{selected: (currentOption.value == item.value)}\">\n" +
    "<li ng-class=\"{'index-1':item.index===1 && !readonlyMode,'index-2':item.index===2 && !readonlyMode}\">\n" +
    "<span ng-if=!readonlyMode ng-class=\"{'ul-close':item.isClosed,visible:visibleProvider[$index+1].index > item.index || item.hasChildren}\" class=add-arrow ng-click=openClose($event,item)></span>\n" +
    "<div class=item-detail ng-class=\"{check:!readonlyMode, checked:item.isChecked, unchecked:!item.isChecked && !item.indeterminate, indeterminate:item.indeterminate}\" ng-click=selectItem(item);$event.stopPropagation();$event.preventDefault();>{{::item[displayProperty]}}</div>\n" +
    "</li>\n" +
    "</div>\n" +
    "</div>"
  );

}]);
