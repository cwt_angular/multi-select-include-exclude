'use strict';
(function (module) {
    try {
        module = angular.module('cwt.multi-select-include-exclude');
    } catch (e) {
        module = angular.module('cwt.multi-select-include-exclude', []);
    }
    var directiveName = 'virtualListIncludeExclude';
    var theDirective = function ($compile) {
        return {
            restrict: 'E',
            require: 'ngModel',
            templateUrl: 'templates/ui-virtual-list-include-exclude.html',
            scope: {
                uiDataProvider: '=',
                onlySelected: '=',
                searchCrit: '=',
                onSelect: '&',
                readonlyMode: '=',
                disabled: '=',
                ngModel: '=',
                displayProperty: '@'
            },
            link: function (scope, elem, attrs, ngModelCtrl) {
                var rowHeight = 30;
                scope.height = 200;
                scope.scrollTop = 0;
                scope.visibleProvider = [];
                scope.cellsPerPage = 0;
                scope.numberOfCells = 0;
                scope.canvasHeight = {};

                var allChilds;

                function update() {
                    allChilds = getAllchilds(scope.uiDataProvider, [], 0);
                    if (scope.selectAll) {
                        scope.selectAll = false;
                        _.each(allChilds, function (child) {
                            scope.selectItem(child);
                        });
                    } else {
                        scope.init();
                    }
                }


                scope.$watch('uiDataProvider', function ui() {
                    update();
                });
                scope.$watch('onlySelected', function onlyselected() {
                    update();
                });
                scope.$watch('searchCrit', function searchcrit() {
                    update();
                });
                scope.$watch('ngModel', function searchcrit() {
                    update();
                });

                // Init
                scope.init = function () {
                    elem[0].removeEventListener('scroll', scope.onScroll);
                    elem[0].addEventListener('scroll', scope.onScroll);
                    if (allChilds) {
                        scope.cellsPerPage = Math.round(scope.height / rowHeight);
                        scope.numberOfCells = 3 * scope.cellsPerPage;
                        scope.canvasHeight = {
                            height: allChilds.length * rowHeight + 'px'
                        };

                        scope.updateDisplayList();
                    }
                };

                scope.updateDisplayList = function () {
                    var firstCell = Math.max(Math.floor(scope.scrollTop / rowHeight) - scope.cellsPerPage, 0);
                    var cellsToCreate = Math.min(firstCell + scope.numberOfCells, scope.numberOfCells);
                    scope.visibleProvider = allChilds.slice(firstCell, firstCell + cellsToCreate);

                    for (var i = 0; i < scope.visibleProvider.length; i++) {
                        scope.visibleProvider[i].styles = {
                            'top': ((firstCell + i) * rowHeight) + 'px'
                        };
                    }
                };

                function checkChildren(item, state) {
                    if (item.children && item.children.length > 0) {
                        _.forEach(item.children, function (child) {
                            child.isChecked = item.isChecked;
                            if (child.children) {
                                checkChildren(child, state);
                            }
                        });
                    }
                }

                scope.selectItem = function (item) {
                    if (!scope.disabled && !scope.readonlyMode) {
                        item.isChecked = !item.isChecked;
                        checkChildren(item, item.isChecked);
                        scope.onSelect({ item: item, value: item.isChecked});
                        scope.updateDisplayList();
                    }
                };

                scope.openClose = function ($event, item) {
                    item.isClosed = !item.isClosed;
                    allChilds = getAllchilds(scope.uiDataProvider, [], 0);
                    scope.init();
                    $event.stopPropagation();
                    $event.preventDefault();
                };
                function getAllchilds(item, childs, index, force) {
                    var hulp = [];
                    _.forEach(item, function (child) {
                        child.index = index;
                        if (child.children && child.children.length !== 0 && !force) {
                            child.hasChildren = child.children.length > 0;
                            if (scope.searchCrit) {
                                child.isClosed = false;
                            }

                            if (scope.onlySelected && !scope.readonlyMode) {
                                child.isClosed = false;
                            } else if (child.isClosed && !scope.readonlyMode) {
                                var test = getAllchilds([child], [], index, true);
                                if (test.length === 1) {
                                    hulp.push(child);
                                }
                                return;
                            }
                            var children = getAllchilds(child.children, childs, index + 1);
                            if (children.length > 0) {
                                if (!scope.readonlyMode) {
                                    hulp.push(child);
                                }
                                hulp = hulp.concat(children);
                            } else if (child.children.length === 0) {
                                if (scope.onlySelected || scope.readonlyMode) {
                                    if (child.isChecked) {
                                        hulp = hulp.concat(child);
                                    }
                                } else {
                                    hulp = hulp.concat(child);
                                }
                            }

                        } else {
                            if (scope.onlySelected || scope.readonlyMode) {
                                if (!child.isChecked) {
                                    return;
                                }
                            }
                            else {
                                if (child.isChecked && !scope.searchCrit) {
                                    return;
                                }
                            }
                            if (scope.searchCrit && scope.searchCrit !== '') {
                                if (child && child[scope.displayProperty] && child[scope.displayProperty].toLowerCase().indexOf(scope.searchCrit.toLowerCase()) === -1) {
                                    return;
                                }
                                if (child && !child[scope.displayProperty]) {
                                    return;
                                }
                            }
                            hulp.push(child);
                        }
                    });
                    return hulp;
                }
                scope.$on('destroy', function () {
                    elem[0].removeEventListener('scroll', scope.onScroll);
                });
                scope.onScroll = function (evt) {
                    scope.$apply(function () {
                        scope.scrollTop = elem.prop('scrollTop');
                        scope.updateDisplayList();
                    });
                };

                scope.init();
            }
        };
    };
    theDirective.$inject = ['$compile'];
    module.directive(directiveName, theDirective);
})(null);
